import warnings

from .base import (
    TuteriaApiException,
    get_field_value)
from . import (BaseClient, FormField, BaseGraphClient)


class RewardGraph(BaseGraphClient):
    path = 'graphql'

    def cached_milestones_query(self):
        query = """
        {
            cached_milestones{
                condition
                score
                absolute_url
            }
        }
        """
        if not self.change:
            query = """
            query getMilestones
            """ + query
        return query

    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)
            return self.client.execute(query, first_args)
        return self.client.execute(*args)

    def get_cached_milestones(self):
        result = self._execute(
            self.cached_milestones_query(),
            {}, "getMilestones")
        return self._get_response(result, "cached_milestones")


class RewardAPIClient(BaseClient):
    path = ''

    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")

        self.service = RewardGraph(self.base_url)
        self.get_request_class()

    def get_milestones(self, **kwargs):
        result = self.service.get_cached_milestones(**kwargs)
        return result
