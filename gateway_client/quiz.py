# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import importlib

from builtins import super, str

from . import (BaseClient, TuteriaApiException)

try:
    http = importlib.import_module("django.http")
    Http404 = http.Http404
except ImportError as e:
    class Http404(Exception):
        pass

class QuizAPIClient(BaseClient):
    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url

        if not self.base_url:
            raise TuteriaApiException(
                "Did you forget to pass the server url or"
                " set it as an environmental variable API_GATEWAY_SERVER_URL")
        self.get_request_class()

    def _fetch_request(self, method, path, **kwargs):

        path = str(path)

        return super()._fetch_request(method, path, **kwargs)

    def get_categories(self):
        """
        :returns: list of categories
        """
        return self._fetch_request("GET", "quiz-service/categories/")

    def get_subcategories(self):
        """
        :returns: list of sub_categories
        """
        return self._fetch_request("GET", "quiz-service/subcategories/")

    def fetch_skills_in_category(self, category_slug):
        """
        :param category_slug: The slug of the category to be fetched

        :returns: dictionary of category details 

        :raises AssertionError: if category_slug is None or len(category_slug) < 1
        """

        assert category_slug is not None
        assert len(category_slug) > 0
        path = "quiz-service/categories/%s/skills/" % category_slug

        try:
            response = self._fetch_request("GET", path)
        except Http404 as e:
            raise TuteriaApiException("Category was not found")

        return response['data']

    def fetch_subcategories_in_category(self, sub_category_slug):
        """
        :param subcategory_slug: The slug of the category to be fetched

        :returns: dictionary of subcategory details 

        :raises AssertionError: if sub_category_slug is None or len(sub_category_slug) < 1
        """

        assert sub_category_slug is not None
        assert len(sub_category_slug) > 0
        path = "quiz-service/categories/%s/" % sub_category_slug

        try:
            response = self._fetch_request("GET", path)
        except Http404 as e:
            raise TuteriaApiException("SubCategory was not found")

        return response

    def get_quiz(self, quiz_url):
        """
        :param quiz_url: The slug of the quiz

        :returns: a dictionary containing quiz data

        :raises AssertionError: if quiz_url is None or len(quiz_url) < 1
        """
        assert quiz_url is not None
        assert len(quiz_url) > 0
        path = "quiz-service/quiz/%s/" % quiz_url

        try:
            response = self._fetch_request("GET", path)
        except Http404 as e:
            print(e)
            raise TuteriaApiException("Quiz was not found")

        return response['data']

    def start_quiz(self, quiz_url, tutor_id):
        """
        :param quiz_url: The slug of the quiz

        :returns: a dictionary containing quiz data

        :raises AssertionError: if quiz_url is None or len(quiz_url) < 1 tutor_id is None or len(tutor_id) < 1
        """
        assert quiz_url is not None
        assert len(quiz_url) > 0
        assert tutor_id is not None
        # assert len(tutor_id) > 0
        payload = {"tutor": tutor_id}

        path = "quiz-service/quiz/%s/started/" % quiz_url

        try:
            response = self._fetch_request("POST", path, json=payload)
        except Http404 as e:
            raise TuteriaApiException("The Specified Quiz was not found")
        return response['data']

    def complete_quiz(self, quiz_url, tutor_id, score):
        """
        :param quiz_url: The slug of the quiz

        :returns: a dictionary containing quiz data

        :raises AssertionError: if quiz_url is None or len(quiz_url) < 1 tutor_id is None or len(tutor_id) < 1 and score is None or len(score) < 1
        """
        assert quiz_url is not None
        assert len(quiz_url) > 0
        assert tutor_id is not None
        assert score is not None

        payload = {"tutor": tutor_id, "result": score}

        path = "quiz-service/quiz/%s/completed/" % quiz_url

        try:
            response = self._fetch_request("POST", path, json=payload)
        except Http404 as e:
            raise TuteriaApiException("The specified Quiz was not found")

        return response

    def get_statistics(self, tutor, slug):
        """
        :param slug: The slug of the subject

        :returns: a dictionary containing quiz data

        :raises AssertionError: if slug is None or tutor is None
        """

        assert slug is not None
        assert len(slug) > 0
        assert tutor is not None

        path = "quiz-service/sitting/%s/subject/%s/" % (tutor, slug)

        try:
            response = self._fetch_request("GET", path)
        except Http404 as e:
            raise TuteriaApiException(
                "The specified tutor hasn't taken this Quiz")
        return response

    def get_survey(self, slug):
        """
        :param slug: The slug of the subject

        :returns: a dictionary containing data

        :raises AssertionError: if slug is None
        """

        assert slug is not None
        assert len(slug) > 0

        path = "quiz-service/skill/%s/survey/" % (slug)

        try:
            response = self._fetch_request("GET", path)
        except Http404 as e:
            raise TuteriaApiException("The specified survey was not found")
        return response

    def get_skill(self, slug):
        """
        :param quiz_url: The slug of the skill

        :returns: a dictionary containing skill data

        :raises AssertionError: if slug is None or len(slug) < 1

        :raises TuteriaApiException if Skill does not exist
        """
        assert slug is not None
        assert len(slug) > 0
        path = "quiz-service/skill/%s/" % slug

        try:
            response = self._fetch_request("GET", path)
        except Http404 as e:
            raise TuteriaApiException("Skill was not found")

        return response

    def create_category(self, name):
        """
        :param name: The slug of the category

        :returns: a dictionary containing category instance
        """

        path = "quiz-service/categories/"
        data = {
            "name": name,
        }
        response = self._fetch_request("POST", path, json=data)

        return response

    def create_skill(self, category_id, name):
        """
        :param category_id: The id of the category

        :param name: The name of the skill to create
        :returns: a dictionary containing category instance
        """

        path = "quiz-service/skill/"
        data = {
            "name": name,
            "category_id": category_id
        }

        response = self._fetch_request("POST", path, json=data)

        return response

    def create_subcategory(self, category_id, name):
        """
        :param category_id: The id of the category

        :param name: The name of the subcategory to create
        :returns: a dictionary containing subcategory instance
        """

        path = "quiz-service/subcategories/"
        data = {
            "name": name,
            "category_id": category_id
        }

        response = self._fetch_request("POST", path, json=data)

        return response

    def create_quiz(self, title,skill):
        """
        :param title: title of the quiz

        :param skill: the skill for which the quiz to be created
        :returns: a dictionary containing quiz instance
        """

        path = "quiz-service/quiz/"
        data = {
            "title":title,
            "skill_id":skill

        }
        response = self._fetch_request("POST", path, json=data)

        return response
