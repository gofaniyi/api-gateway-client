try:
    from urllib import urlencode
except ImportError:
    from urllib.parse import urlencode
import os
import json
import requests
from .client3 import GraphQLClient
import importlib
import decimal


class TuteriaApiException(Exception):
    pass


class TuteriaDetail:
    email = ' info@tuteria.com'
    sales_email = 'sales@tuteria.com'
    help_email = 'help@tuteria.com'
    help_phone = '08091547961'
    phone_number = ' 090 945 26878'
    mobile_number = ' 090 945 26878'
    address = ' Lagos, Nigeria'
    gt = dict(account='0169418080', name='Tuteria Company')
    uba = dict(account='1018919564', name='Tuteria Company')
    first_bank = dict(account='1234567890', name='Tuteria Company')
    stanbic = dict(account='1234567890', name='Tuteria Company')
    criminal_cost = 9000.0
    address_cost = 7500.0
    both_cost = criminal_cost + address_cost
    site_logo = os.getenv('TUTERIA_LOGO', '')
    processing_fee = 2500


def construct_graphql_query(dict_object):
    """
    {"name": "wallet",
    "key": "username",
    "fields": ["owner", "upcoming_earnings",
    {'name': "transactions",
    'key': None,
    "fields": ["display", "type"] }]}
    :returns
        query{
            wallet(username:""){
                total_earned,
                total_withdrawn,
                total_credit_used_to_hire,
                total_used_to_hire,
                total_payed_to_tutor,
                upcoming_earnings,
                transactions{
                    total,
                    type,
                    display,
                    to_string,
                    amount
                }
            }
        }
    """
    base = resolve_graphql_field(dict_object, dict_object.get('value'))
    return "query{\n%s}\n" % base


def resolve_graphql_field(field, value=None):
    """Returns graphql query as a string"""
    if isinstance(field, str):
        return "%s,\n" % field
    # assuming it is a dict
    base = "%s{\n" % field['name']
    if value:
        base = "%s(%s:\"%s\"){\n" % (
            field['name'], field['key'], field['value'])
    for the_field in field['fields']:
        base += resolve_graphql_field(the_field, get_value(the_field))
    base += "}\n"
    return base


def get_value(val):
    if isinstance(val, str):
        return val
    return val.get("value")


def get_field_value(value):
    """Return graphql dictionary representation"""
    if isinstance(value, str):
        return value
    return construct_graphql_dict(value['name'], value['fields'],
                                  key=value.get('key'), value=value.get('value'))


def construct_graphql_dict(name, fields, key=None, value=None):
    """Constructs a dictionary of the graphql query"""
    options = {
        'name': name,
        'key': key,
        'fields': [get_field_value(x) for x in fields]
    }
    if value:
        options.update(value=value)
    return options


class FormServiceMixin(object):

    def validate(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}validate_form/".format(value)
        return self._fetch_request('POST', path,
                                   json=cleaned_data, params=kwargs)

    def get_form_parameters(self, **kwargs):
        sid, kwargs = self.resolve_path(**kwargs)
        result = self._fetch_request('OPTIONS', sid, params=kwargs)
        return self._form_field_result(result['form_fields'])

    def _form_field_result(self, result):
        return FormField(result)

    def save_form(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}save_form/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)


class BaseClient(FormServiceMixin):
    """Base client class for all graphql queries"""
    base_url = os.getenv("API_GATEWAY_SERVER_URL", "")
    config = {'AMQP_URI': os.getenv("BROKER_URL", "")}
    path = ''
    request_library = requests

    def get_request_class(self):
        if self.base_url == '/':
            Client = importlib.import_module("django.test")
            http = importlib.import_module("django.http")
            Http404 = http.Http404

            class NewClient(Client.Client):
                def raise_for_status(self, res_status, status=400):
                    if status:
                        if res_status > status:
                            raise Http404("Error greater than 400")
                    else:
                        if res_status > 400:
                            raise Http404("Error greater than 400")
            self. request_library = NewClient()

    def _query_graphql_server(self, query):
        new_query = construct_graphql_query(query)
        # import pdb
        # pdb.set_trace()
        response = requests.post(
            self.base_url, json={
                'query': new_query, 'variables': None})
        response.raise_for_status()
        return response.json()['data']

    def _fetch_request(self, method, path, status=None, **kwargs):
        url = self.base_url + self.path + path
        action = {
            'POST': self.request_library.post,
            'OPTIONS': self.request_library.options,
            'GET': self.request_library.get,
            'PATCH': self.request_library.patch,
        }
        new_kwargs = self._update_kwargs(method, kwargs)
        if self.base_url == "/":
            params = new_kwargs.pop('params', {})
            url = url + "?" + urlencode(params)
            new_kwargs.update(content_type="application/json")
        response = action[method](url, **new_kwargs)
        self.throw_exception(response, status)
        return response.json()

    def throw_exception(self, response, status=None):
        if self.base_url == "/":
            self.request_library.raise_for_status(
                response.status_code, status
            )
        else:
            if status:
                if response.status_code > status:
                    response.raise_for_status()
            else:
                response.raise_for_status()

    def __decimal_default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return float(obj)
        raise TypeError

    def _update_kwargs(self, method, kwargs):
        data = kwargs.pop('json', None)
        if data:
            data = json.dumps(data, default=self.__decimal_default)
        if self.base_url == "/":
            if method == 'GET':
                data = kwargs.pop('params', None)
            kwargs.update(data=data)
        else:
            if data or method == 'POST':
                kwargs['json'] = json.loads(data) if data else {}
        return kwargs



class FormField:

    def __init__(self, dictionary):

        for k, v in dictionary.items():
            setattr(self, k, (k, v))

    def get_fields(self):
        result = [key for key, value in self.__dict__.items()
                  if type(value) == tuple]
        return result


class BaseGraphClient(object):
    path = ""
    env_variable = 'GRAPHQL_ENDPOINT'

    def __init__(self, url="", import_path="config.schema"):
        if isinstance(url, str):
            self.change = False
            self.base_url = os.environ.get(self.env_variable)
            if url:
                self.base_url = url
            if not self.base_url:
                raise TuteriaApiException(
                    "Url for Graphql Service was not found.")
            if url in ['/', '/graphql']:
                schema = importlib.import_module(import_path)
                self.client = schema.schema
                self.change = True
            else:
                self.client = GraphQLClient(self.base_url + self.path)
        else:
            self.change = True
            self.client = url

    
    def _transform_query(self, *args):
        query = args[0]
        first_args = args[1]
        new_query = query
        for key in first_args.keys():
            value = first_args[key]
            if value:
                value = json.dumps(value)
            else:
                value = json.dumps("")
            new_query = new_query.replace(
                ":${}".format(key),
                ":{}".format(value))
        return new_query, first_args

    def _execute(self, query, *args):
        if self.change:

            return self.client.execute(query, context_value=args[0])
        return self.client.execute(query, *args)

    def _get_response(self, result, *args):
        if self.change:
            response = result.data
        else:
            response = result['data']
        for i in range(len(args)):
            response = response.get(args[i])
            if not response:
                break
        return response


def get_url(settings_url, graphql=True):
    """Generates the corresponding endpoint for either
    monolith or service 

    Args:
        settings_url (str/object): the endpoint 
        graphql (bool, optional): if the endpoint is a graphql query

    Returns:
        str/object: A string representing the full endpoint or 
        a schema obj
    """
    result = settings_url
    if 'schema' in settings_url:
        ss = importlib.import_module(settings_url)
        result = ss.schema
    else:
        if graphql:
            result = settings_url + "/graphql"
    return result
