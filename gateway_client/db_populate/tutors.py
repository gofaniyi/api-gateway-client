from . import BaseTransferClient, populate
import importlib
from gateway_client.image import ImageAPIClient


def new_populate(cls, fields, data, extras=[]):
    w = populate(cls, fields, data, extras)
    w.username = data.slug
    return w


class DependsOnUserClient(BaseTransferClient):
    user_id = 'tutor_id'
    exclude_fields = ['owner', 'tutor', 'user', 'milestone']
    user_ids = []
    include_fields = ['tutor_id']

    def condition(self, d):
        return getattr(d, self.user_id) in self.user_ids

    def get_sql_query(self):
        return (
                'SELECT "{0}".* FROM "{0}" '
                'INNER JOIN "auth_user" ON ("{0}"."{1}" = '
                '"auth_user"."id") INNER JOIN "users_userprofile" ON ('
                '"auth_user"."id" = "users_userprofile"."user_id") WHERE '
                '("users_userprofile"."application_status" > 0 AND NOT '
                '("{0}"."{1}" IS NULL))'.format(self.db_name, self.user_id))

    def get_foreign_keys(self):
        return [self.user_id]

    def populate_data(self):
        from django.db import IntegrityError
        fields = self.get_fields()
        data = [populate(self.model, fields, d) for d in self.get_query()
                if self.condition(d)]
        self.model.objects.bulk_create(data)
        print("{} Dump Completed".format(self.model.__class__.__name__))

    # def populate_data(self):
    #     from django.db import IntegrityError
    #     import pdb;pdb.set_trace()
    #     fields = self.get_fields()

    #     data = [populate(self.model, fields, d) for d in self.get_query()
    #             if self.condition(d)]
    #     self.model.objects.bulk_create(data)
    #     print("{} Dump Completed".format(self.model.__class__.__name__))
    #     query = [d for d in self.get_query() if self.condition(d)]
    #     for rec in query:
    #         param = {key:getattr(rec, key) for key in self.get_foreign_keys()}
    #         try:
    #             self.model.objects.filter(
    #                 pk=rec.id).update(**param)
    #         except IntegrityError as e:
    #             pass
    #     print("{} Dump Completed".format(self.model.__class__.__name__))
        

class DependsOnTutorSkill(DependsOnUserClient):
    user_id = 'ts_id'
    exclude_fields = ['ts','tutor_skill']
    user_ids = []


    def get_sql_query(self):
        return "SELECT * from {}".format(self.db_name)


# def populate_all_subjects_data(settings, no_to_populate=None, import_path="tutor_service.users.models"):
#     models = importlib.import_module(import_path)
#     TutorSkill = models.TutorSkill
#     QuizSitting = models.QuizSitting
#     SkillCertificate = models.SkillCertificate
#     SubjectExhibition = models.SubjectExhibition

#     class TutorSkillTransferClient(BaseTransferClient):
#         model = TutorSkill
#         exclude_fields = ['sitting', 'exhibitions', 'skillcertificate', 'tutor']
#         db_name = "skills_tutorskill"
#         include_fields = ['tutor_id']


#     class SubjectExhibitionTransferClient(DependsOnTutorSkill):
#         model = SubjectExhibition
#         db_name = "skills_subjectexhibition"
#         include_fields = ['ts_id']
#         user_ids = TutorSkill.objects.values_list('pk', flat=True)


#     class SkillCertificateTransferClient(DependsOnTutorSkill):
#         model = SkillCertificate
#         db_name = "skills_skillcertificate"
#         include_fields = ['ts_id']
#         user_ids = TutorSkill.objects.values_list('pk', flat=True)

#     class QuizSittingTransferClient(DependsOnTutorSkill):
#         model = QuizSitting
#         user_id = 'tutor_skill_id'
#         db_name = "skills_quizsitting"
#         include_fields = ['tutor_skill_id']
#         user_ids = TutorSkill.objects.values_list('pk',flat=True)

#     tt = TutorSkillTransferClient(settings.HOST_URL, no_to_populate)
#     tt.populate_data()
#     # se = SubjectExhibitionTransferClient(settings.HOST_URL, no_to_populate)
#     # se.populate_data()
#     # st = SkillCertificateTransferClient(settings.HOST_URL, no_to_populate)
#     # st.populate_data()
#     # qt = QuizSittingTransferClient(settings.HOST_URL, no_to_populate)
#     # qt.populate_data()


def populate_tutors_info(settings,no_to_populate=None, import_path="tutor_service.users.models"):
    models = importlib.import_module(import_path)

    class TutorBasicProfileTransferClient(BaseTransferClient):
        model = models.User
        db_name = "auth_user"
        exclude_fields = [
            'phonenumber', 'education', 'workexperience', 'guarantor', 'identifications', 'milestones', 'tutorskill',
            'image', 'applications', 'auth_token', 'name',
            'socialaccounts', 'created_advanced_filters', 'advancedfilter', 'logentry',
        ]

        def get_sql_query(self):
            return ('SELECT "auth_user".*, "users_userprofile".*, "users_userprofile".custom_header as title, "account_emailaddress".verified as email_verified '
                    ' FROM "auth_user" '
                    'INNER JOIN "users_userprofile" ON ("auth_user".id = "users_userprofile".user_id) '
                    'INNER JOIN "account_emailaddress" ON ("auth_user".email = "account_emailaddress".email) '
                    'WHERE "users_userprofile".application_status > 0 '
                    'ORDER BY "users_userprofile".user_id') 
            
        def populate_data(self):
            fields = self.get_fields()
            data = [new_populate(self.model, fields, d)
                    for d in self.get_query()]
            self.model.objects.bulk_create(data)
            print("{} Dump Completed".format(self.model.__class__.__name__))
            
                
    class PhoneNumberTransferClient(DependsOnUserClient):
        model = models.PhoneNumber
        db_name = "users_phonenumbers"
        user_id = 'owner_id'
        user_ids = models.User.objects.values_list('pk', flat=True)
        include_fields = ['owner_id']

    class EducationTransferClient(DependsOnUserClient):
        model = models.Education
        db_name = "tutors_educations"
        user_ids = models.User.objects.values_list('pk', flat=True)
        exclude_fields = DependsOnUserClient.exclude_fields +['country', 'certificate']
        

    class WorkExperienceTransferClient(DependsOnUserClient):
        model = models.WorkExperience
        db_name = "tutor_work_experiences"
        user_ids = models.User.objects.values_list('pk', flat=True)
        exclude_fields = DependsOnUserClient.exclude_fields + ['is_private']


    class GurantorTransferClient(DependsOnUserClient):
        model = models.Guarantor
        db_name = "tutor_guarantors"
        user_ids = models.User.objects.values_list('pk', flat=True)

    class UserIdenticationTransferClient(DependsOnUserClient):
        model = models.UserIdentification
        db_name = "users_identification"
        user_id = 'user_id'
        user_ids = models.User.objects.values_list('pk', flat=True)
        exclude_fields = DependsOnUserClient.exclude_fields + ['identity']
        include_fields = ['user_id']

        
    class MilestoneTransferClient(BaseTransferClient):
        model = models.Milestone
        db_name = "rewards_milestone"
        exclude_fields = ['usermilestone']


    class UserMiletoneTransferClient(DependsOnUserClient):
        model = models.UserMilestone
        db_name = "users_usermilestone"
        user_id = 'user_id'
        user_ids = models.User.objects.values_list('pk', flat=True)
        include_fields = ['user_id', 'milestone_id']
        
    class TutorSkillTransferClient(DependsOnUserClient):
        model = models.TutorSkill
        exclude_fields = ['sitting', 'exhibitions', 'tutor']
        db_name = "skills_tutorskill"
        user_ids = models.User.objects.values_list('pk', flat=True)


        def get_sql_query(self):
            return ('SELECT d.*, c.slug as skill_slug, '
                    'json_agg(row_to_json( (SELECT r FROM (SELECT k.award_name, k.award_institution) r) )) as certificates '
                    'FROM "skills_tutorskill" d '
                    'INNER JOIN "auth_user" a ON (d.tutor_id = a.id) '
                    'INNER JOIN "users_userprofile" b ON (a.id = b.user_id) '
                    'INNER JOIN "skills_skill" c ON (d.skill_id = c.id) '
                    'INNER JOIN "skills_skillcertificate" k ON (d.id = k.ts_id) '
                    'WHERE (b."application_status" > 0 AND NOT (d.tutor_id IS NULL)) '
                    'group by k.ts_id, d.id, c.slug')


    # class SubjectExhibitionTransferClient(DependsOnTutorSkill):
    #     model = models.SubjectExhibition
    #     db_name = "skills_subjectexhibition"
    #     include_fields = ['ts_id']
    #     user_ids = models.TutorSkill.objects.values_list('pk', flat=True)


    # class SkillCertificateTransferClient(DependsOnTutorSkill):
    #     model = models.SkillCertificate
    #     db_name = "skills_skillcertificate"
    #     include_fields = ['ts_id']
    #     user_ids = models.TutorSkill.objects.values_list('pk', flat=True)


    # class QuizSittingTransferClient(DependsOnTutorSkill):
    #     model = models.QuizSitting
    #     user_id = 'tutor_skill_id'
    #     db_name = "skills_quizsitting"
    #     include_fields = ['tutor_skill_id']
    #     user_ids = models.TutorSkill.objects.values_list('pk',flat=True)

    aa = TutorBasicProfileTransferClient(settings.HOST_URL,no_to_populate=no_to_populate)
    aa.populate_data()

    bb = PhoneNumberTransferClient(settings.HOST_URL)
    bb.populate_data()

    cc = EducationTransferClient(settings.HOST_URL)
    cc.populate_data()
    
    dd = WorkExperienceTransferClient(settings.HOST_URL)
    dd.populate_data()

    ee = GurantorTransferClient(settings.HOST_URL)
    ee.populate_data()

    ff = UserIdenticationTransferClient(settings.HOST_URL)
    ff.populate_data()

    gg = MilestoneTransferClient(settings.HOST_URL)
    gg.populate_data()

    hh = UserMiletoneTransferClient(settings.HOST_URL)
    hh.populate_data()

    ii = TutorSkillTransferClient(settings.HOST_URL)
    ii.populate_data()

    # jj = SubjectExhibitionTransferClient(settings.HOST_URL)
    # jj.populate_data()

    # kk = SkillCertificateTransferClient(settings.HOST_URL)
    # kk.populate_data()

    # ll = QuizSittingTransferClient(settings.HOST_URL)
    # ll.populate_data()


def migrate_info_into_data_dump(settings, no_to_populate=None, import_path="users.models"):
    models = importlib.import_module(import_path)

    class TutorInfoTransferClient(BaseTransferClient):
        model = models.User
        
        def get_query(self):
            users = self.model.objects.filter(profile__application_status__range=[0,3])
            if self.no_to_populate:
                return users[self.no_to_populate:]
            return users

        def populate_data(self):
            from django.forms.models import model_to_dict
            fields = self.model._meta.get_fields()

            include_fields = ['email', 'first_name', 'last_name', 'profile',
                              'country', 'teach_online', 'location', 'phonenumber']

            related_fields = [{'related_name': 'location_set', 'key': 'locations', 'model': 'location', 
                               'fields': ['address', 'state', 'distances', 'vicinity', 'vicinity_type', 'addr_type', 'longitude', 'latitude']},
                            {'related_name': 'phonenumber_set', 'key': 'phone_numbers', 'model': 'phonenumber', 
                            'fields' : ['number', 'verified', 'primary']}]

            profile_include_fields = ['description', 'gender', 'dob', 'custom_header', 
            'address_reason', 'tutor_description', 'years_of_teaching', 'classes', 
            'curriculum_used', 'curriculum_explanation','potential_subjects', 'levels_with_exams', 'answers']

            users = (i for i in self.get_query())

            for user in users:
                data_dump = {}
                escape = []
                for f in fields:
                    if f.name in include_fields:
                        if not f.one_to_one and not f.many_to_one and not f.many_to_many and not f.one_to_many:
                            value = getattr(user, f.name)
                            if f.name in ['country']:
                                value = str(value)

                            data_dump[f.name] = value
                        elif f.one_to_one:
                            if f.name == 'profile':
                                profile = user.profile
                                profile = model_to_dict(profile)
                                for k, val in profile.items():
                                    if k in profile_include_fields:
                                        if k == 'dob':
                                            val = val.strftime('%Y-%m-%d') if val else ''
                                        data_dump[k] = val
                        else:

                            key = next(
                                (i for i in related_fields if i['model'] == f.name), None)
                            if key:
                                value = getattr(user, key['related_name']).all()
                                #convert value to list of dictionaries
                                data_dump[key['key']] = list(value.values(*key['fields']))
                
                setattr(user, 'data_dump', data_dump)
                user.save()
            print("{} Dump Completed".format(self.model.__class__.__name__))

    ii = TutorInfoTransferClient(settings.HOST_URL, no_to_populate=no_to_populate)
    ii.populate_data()
