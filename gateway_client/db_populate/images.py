import importlib
import ntpath
from cloudinary.models import CloudinaryField
from gateway_client.tutor import TutorAPIClient
from . import BaseTransferClient



def populate_image(cls, fields, data, extras=[]):
    w = cls()
    for o in fields:
        if o == 'image':
            image_val = getattr(data, o)
            #Get the Cloudinary Resource from the image_val
            val = CloudinaryField().parse_cloudinary_resource(image_val)
            setattr(w, 'public_id', val.public_id)
        else:
            val = getattr(data, o)
        setattr(w, o, val)
    return w


def populate_all_data(settings,no_to_populate=None, import_path="image_service.images.models"):
    models = importlib.import_module(import_path)
    
    class TutorProfileImageTransferClient(BaseTransferClient):
        model = models.Image
        db_name = "users_userprofile"
        user_id = "user_id"
        image_keyname = 'image'
        exclude_fields = ['id', 'storage', 'public_id', 'object_id']
        tutors_image_info = []

        def get_sql_query(self):
            return ("SELECT b.image, b.user_id, a.email, a.slug FROM auth_user a "
                    "INNER JOIN users_userprofile b ON (a.id = b.user_id) "
                    "INNER JOIN account_emailaddress f ON (a.email = f.email) "
                    "WHERE b.application_status > 0 "
                    "and b.image <> '' and b.image is not null ORDER BY b.user_id")

        def populate_data(self):
            fields = self.get_fields()
            fields += ['slug', 'email']
            data = [populate_image(self.model, fields, d) for d in self.get_query()]

            try:
                tutor_service = TutorAPIClient(settings.TUTOR_SERVICE_URL)
            except:
                pass

            for each in data:    
                try:
                    user = tutor_service.get_user(username=each.slug)
                    if user: #Only if this user exist in the tutor service are we going to save its image id.
                        payload = {
                            'image' : each.object_id,
                            'slug' : each.slug,
                            'email' : each.email
                        }
                        tutor_service.create_user(payload)
                        each.save()
                except Exception as e:
                    pass

            #self.model.objects.bulk_create(data)
            print("{} Dump Completed".format(self.model.__class__.__name__))

    class TutorIdentificationImageTransferClient(BaseTransferClient):
        model = models.Image
        db_name = ""
        user_id = ""
        image_keyname = ''
        exclude_fields = ['id', 'storage', 'public_id', 'object_id', 'user_id']

        def get_sql_query(self):
            return ("SELECT b.id as unique_id, b.identity as image, b.user_id, a.slug FROM auth_user a "
                    "INNER JOIN users_userprofile c ON (a.id = c.user_id) "
                    "INNER JOIN account_emailaddress f ON (a.email = f.email) "
                    "INNER JOIN users_identification b ON (a.id = b.user_id) "
                    "WHERE c.application_status > 0 and b.identity <> '' and b.identity is not null "
                    "ORDER BY b.user_id ASC ")

        def populate_data(self):
            fields = self.get_fields()
            fields += ['slug', 'unique_id']
            data = [populate_image(self.model, fields, d) for d in self.get_query()]
            tutor_service = None

            try:
                tutor_service = TutorAPIClient(settings.TUTOR_SERVICE_URL)
            except:
                pass

            for each in data:
                try:
                    user = tutor_service.get_user(username=each.slug)
                    if user: #Only if this user exist in the tutor service are we going to save its image id.
                        payload = {
                            'identity' : each.object_id,
                            'id' : each.unique_id
                        }
                        tutor_service.save_user_identification(payload, username=each.slug)
                        each.save()
                except Exception as e:
                    pass

            print("{} Dump Completed".format(self.model.__class__.__name__))


    class TutorEducationCertificateImageTransferClient(BaseTransferClient):
        model = models.Image
        db_name = ""
        user_id = ""
        image_keyname = ''
        exclude_fields = ['id', 'storage', 'public_id', 'object_id', 'user_id']

        def get_sql_query(self):
            return ("SELECT b.id as unique_id, b.certificate as image, b.tutor_id, a.slug FROM auth_user a "
                    "INNER JOIN users_userprofile c ON (a.id = c.user_id) "
                    "INNER JOIN account_emailaddress f ON (a.email = f.email) "
                    "INNER JOIN tutors_educations b ON (a.id = b.tutor_id) "
                    "WHERE c.application_status > 0 and b.certificate <> '' and b.certificate is not null "
                    "ORDER BY b.tutor_id ASC ")

        def populate_data(self):
            fields = self.get_fields()
            fields += ['slug', 'unique_id']
            data = [populate_image(self.model, fields, d) for d in self.get_query()]
            tutor_service = None

            try:
                tutor_service = TutorAPIClient(settings.TUTOR_SERVICE_URL)
            except:
                pass

            for each in data:
                try:
                    user = tutor_service.get_user(username=each.slug)
                    if user: #Only if this user exist in the tutor service are we going to save its image id.
                        payload = {
                            'certificate' : each.object_id,
                            'id' : each.unique_id
                        }
                        tutor_service.update_user_education(payload, username=each.slug)
                        each.save()
                except Exception as e:
                    pass

            print("{} Dump Completed".format(self.model.__class__.__name__))

    class Struct(object):

        def __init__(self, **entries):
            self.__dict__.update(entries)

    class TutorSkillExhibitionsImageTransferClient(BaseTransferClient):
        model = models.Image
        db_name = ""
        user_id = ""
        image_keyname = ''
        exclude_fields = ['id', 'storage', 'public_id', 'object_id', 'user_id']

        def get_sql_query(self):
            return ("SELECT b.ts_id, string_agg(b.image, ',') as images, a.slug FROM auth_user a "
                    "INNER JOIN skills_tutorskill d ON (a.id = d.tutor_id) "
                    "INNER JOIN account_emailaddress f ON (a.email = f.email) "
                    "INNER JOIN skills_subjectexhibition b ON (d.id = b.ts_id) "
                    "GROUP BY b.ts_id, a.slug, a.id ORDER BY a.id ASC ")


        
        def populate_data(self):
            fields = self.get_fields()
            fields += ['slug']

            db_data = self.get_query()
            data = []
            for d in db_data:
                images = getattr(d, 'images')

                if images:
                    bb = {'slug': getattr(d, 'slug'), 'ts_id': getattr(d, 'ts_id'),
                    'image' : '', 'images' : ''} 

                    bb = Struct(**bb)

                    images_arr = images.split(',')
                    images_object_id = []
                    
                    for image in images_arr:
                        try:
                            bb.image = image
                            
                            model = populate_image(self.model, fields, bb)
                            model.save()
                            images_object_id.append(model.object_id)
                        except Exception as e:
                            pass
                    bb.images = images_object_id
                    data.append(bb)

            tutor_service = None
            try:
                tutor_service = TutorAPIClient(settings.TUTOR_SERVICE_URL)
            except:
                pass

            for each in data:
                try:
                    user = tutor_service.get_user(username=each.slug)
                    if user:  # Only if this user exist in the tutor service are we going to save its image id.
                        payload = {
                            'exhibitions': each.images,
                            'id': each.ts_id
                        }
                        tutor_service.update_user_skill(
                            payload, username=each.slug)
                except Exception as e:
                    pass

            print("{} Dump Completed".format(self.model.__class__.__name__))

    ii = TutorProfileImageTransferClient(settings.HOST_URL, no_to_populate=no_to_populate)
    ii.populate_data()

    uu = TutorIdentificationImageTransferClient(settings.HOST_URL, no_to_populate=no_to_populate)
    uu.populate_data()

    uu = TutorEducationCertificateImageTransferClient(settings.HOST_URL, no_to_populate=no_to_populate)
    uu.populate_data()

    uu = TutorSkillExhibitionsImageTransferClient(
        settings.HOST_URL, no_to_populate=no_to_populate)
    uu.populate_data()

