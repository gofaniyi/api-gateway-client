import warnings

from ..base import (
    TuteriaApiException,
    get_field_value)
from .. import (BaseClient, FormField, BaseGraphClient)


class UserGraph(BaseGraphClient):
    path = 'graphql'

    def cached_user_query(self, attributes):
        query = """
        {
            cached_user%s{
                pk
                id
                first_name
                last_name
                submitted_verification
                date_joined
                email_verified
                identity{
                    identity
                    verified
                }
                email
                has_bank{
                    payout_type
                }
                has_mini_calendar
                milestones{
                    milestone{
                        condition
                        score
                    }
                }
                get_absolute_url
                tutor_intent
                profile{
                    image
                    image_approved
                    is_tutor
                    profile_pic
                    registration_level
                    application_status
                }
                reputation
            }
        }
        """ % ("(" + ",".join('%s:$%s' % t for t in zip(attributes, attributes)) + ")" if attributes else "")
        if not self.change:
            query = """
            query getUser
            """ + query
        return query

    def cached_tutor_req_query(self, attributes):
        query = """
            {
                cached_user%s{
                    progress_percentage
                    has_completed_verification
                    next_url
                }
            }
            """ % ("(" + ",".join('%s:$%s' % t for t in zip(attributes, attributes)) + ")" if attributes else "")
        if not self.change:
            query = """
            query getTutorReq
            """ + query
        return query

    def cached_user_locations_query(self, attributes):
        query = """
            {
                cached_user_locations%s{
                    vicinity
                    addr_type
                }
            }
            """ % ("(" + ",".join('%s:$%s' % t for t in zip(attributes, attributes)) + ")" if attributes else "")
        if not self.change:
            query = """
            query getUserLocations
            """ + query
        return query

    def cached_tutor_skills_query(self, attributes):
        query = """
            {
                cached_tutor_skills%s{
                    status
                }
            }
            """ % ("(" + ",".join('%s:$%s' % t for t in zip(attributes, attributes)) + ")" if attributes else "")
        if not self.change:
            query = """
            query getTutorSkills
            """ + query
        return query

    def cached_user_bookings_query(self, attributes):
        query = """
            {
                cached_user_bookings%s{
                    status
                    user{
                        first_name
                    }
                    cancel_initiator{
                        first_name
                    }
                    get_tutor{
                        first_name
                    }
                    skill_display
                    get_absolute_url
                    get_tutor_absolute_url
                }
            }
            """ % ("(" + ",".join('%s:$%s' % t for t in zip(attributes, attributes)) + ")" if attributes else "")
        if not self.change:
            query = """
            query getUserBookings
            """ + query
        return query

    def cached_user_orders_query(self, attributes):
        query = """
            {
                cached_user_orders%s{
                    order
                    status
                    first_session
                    reviewed
                    user{
                        id
                    }
                    cancel_initiator{
                        username
                    }
                    get_tutor{
                        first_name
                    }
                    skill_display
                    get_absolute_url
                }
            }
            """ % ("(" + ",".join('%s:$%s' % t for t in zip(attributes, attributes)) + ")" if attributes else "")
        if not self.change:
            query = """
            query getUserOrders
            """ + query
        return query

    def populate_booking_first_session_query(self):
        query = """
            mutation Mutation{
                populate_booking_first_session(order:$order,user_id:$user_id){
                    booking{
                        order
                        status
                        first_session
                        reviewed
                        user{
                            id
                        }
                        cancel_initiator{
                            username
                        }
                        get_tutor{
                            first_name
                        }
                        skill_display
                        get_absolute_url
                    }
                }
            }
            """
        return query

    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)
            return self.client.execute(query, first_args)
        return self.client.execute(*args)

    def get_cached_user(self, **kwargs):
        result = self._execute(
            self.cached_user_query(kwargs.keys()),
            kwargs, "getUser")
        return self._get_response(result, "cached_user")

    def get_cached_tutor_req(self, **kwargs):
        result = self._execute(
            self.cached_tutor_req_query(kwargs.keys()),
            kwargs, "getTutorReq")
        return self._get_response(result, "cached_user")

    def get_cached_user_locations(self, **kwargs):
        result = self._execute(
            self.cached_user_locations_query(kwargs.keys()),
            kwargs, "getUserLocations")
        return self._get_response(result, "cached_user_locations")

    def get_cached_tutor_skills(self, **kwargs):
        result = self._execute(
            self.cached_tutor_skills_query(kwargs.keys()),
            kwargs, "getTutorSkills")
        return self._get_response(result, "cached_tutor_skills")

    def get_cached_user_bookings(self, **kwargs):
        result = self._execute(
            self.cached_user_bookings_query(kwargs.keys()),
            kwargs, "getUserBookings")
        return self._get_response(result, "cached_user_bookings")

    def get_cached_user_orders(self, **kwargs):
        result = self._execute(
            self.cached_user_orders_query(kwargs.keys()),
            kwargs, "getUserOrders")
        return self._get_response(result, "cached_user_orders")

    def populate_booking_first_session(self, order, user_id):
        result = self._execute(
            self.populate_booking_first_session_query(),
            {"order": order, "user_id": user_id})
        return self._get_response(result, "populate_booking_first_session")


class UserAPIClient(BaseClient):
    path = ''

    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")

        self.service = UserGraph(self.base_url)
        self.get_request_class()

    def _form_field_result(self, result):
        data = {}
        for key, value in result.items():
            data[key] = FormField(value)
        return data

    def get_user(self, **kwargs):
        result = self.service.get_cached_user(**kwargs)
        return result

    def get_user_locations(self, **kwargs):
        result = self.service.get_cached_user_locations(**kwargs)
        return result

    def get_tutor_skills(self, **kwargs):
        result = self.service.get_cached_tutor_skills(**kwargs)
        return result

    def get_user_bookings(self, **kwargs):
        result = self.service.get_cached_user_bookings(**kwargs)
        return result

    def get_user_orders(self, **kwargs):
        result = self.service.get_cached_user_orders(**kwargs)
        return result

    def populate_booking_first_session(self, order, user_id):
        result = self.service.populate_booking_first_session(order, user_id)
        return result

    def get_tutor_req(self, **kwargs):
        result = self.service.get_cached_tutor_req(**kwargs)
        return result
