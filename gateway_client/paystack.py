import requests
import os
from . import TuteriaApiException
import logging

logger = logging.getLogger(__name__)

class PayStack(object):
    def __init__(self, url=None, secret_key=None):
        self.base_url = os.environ.get('PAYSTACK_BASE_URL') or url
        if not self.base_url:
            raise TuteriaApiException("Url for Paystack not set.")
        self.secret_key = os.environ.get('PAYSTACK_SECRET_KEY') or secret_key
        if not self.secret_key:
            raise TuteriaApiException("Secret Key for Paystack not passed")

        self.headers = {'Authorization': 'Bearer %s' % self.secret_key,
                        'Content-Type': 'application/json'}

    def create_customer(self, data):
        r = requests.post(self.base_url +
                          '/customer', json=data, headers=self.headers)
        if r.status_code >= 400:
            raise(requests.HTTPError)
        return r.json()['data']['customer_code']

    def validate_transaction(self, ref):
        r = requests.get(self.base_url +
                         '/transaction/verify/' + ref, headers=self.headers)
        logger.info(r.status_code)
        if r.status_code >= 400:
            logger.info(r.text)
            print(r.text)
            r.raise_for_status()
        print(r.json())
        data = r.json()['data']
        if r.json()['status']:
            return dict(authorization_code=data['authorization']['authorization_code'], amount_paid=data['amount'] / 100)
        return None

    def initialize_transaction(self, data):
        """
        Initializing transaction from server us
        :data : {
            'reference','email','amount in kobo',
            'callback_url'
        }
        """
        r = requests.post(self.base_url +
                          '/transaction/initialize', json=data, headers=self.headers)
        if r.status_code >= 400:
            logger.info(r.text)
            print(r.text)
            r.raise_for_status()
        print(r.json())
        if r.json()['status']:
            return r.json()['data']
        return {}

    def recurrent_charge(self, data):
        """
        When attempting to bill an existing customers that has already paid through us
        :data : {
            'authorization_code','email','amount'
        }
        """
        r = requests.post(self.base_url +
                          '/transaction/charge_authorization', json=data, headers=self.headers)
        if r.status_code >= 400:
            r.raise_for_status()
        print(r.json())
        logger.info(r.json())
        if r.json()['status']:
            return True
        return False
