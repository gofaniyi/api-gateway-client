import warnings
from . import (BaseClient, TuteriaApiException, get_field_value,
               BaseGraphClient)


class LocationGraph(BaseGraphClient):
    path = 'graphql'

    def location_query(self):
        query = """
        {
            user_locations(user_id:$user_id){
                country
                location{
                    id
                    address
                    addr_type
                    full_address
                    area
                    state
                    vicinity
                    longitude
                    latitude
                }
            }
        }
        """
        if not self.change:
            query = """
            query getUserLocations($user_id: Int!)
            """+ query
        return query
    
    def states_query(self):
        query = """
        {
            states
        }
        """
        if not self.change:
            query = """
            query getStates
            """+ query
        return query

    def constituencies_query(self):
        query = """
        {
            constituencies{
                name
                state
            }
        }
        """
        if not self.change:
            query = """
            query getConstituencies
            """+ query
        return query


    def all_locations_query(self, query_variables=None):
        query = """
        {
            all_locations%s{
                user_id
                location{
                    id
                    address
                    addr_type
                    full_address
                    area
                    state
                    vicinity
                    longitude
                    latitude
                }
            }
        }
        """ % ("(" + ",".join('%s:$%s' % t for t in zip(query_variables, query_variables)) + ")" if query_variables else "")
        if not self.change:
            query = """
            query getAllLocations
            """+ query
        return query
    
    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)
            return self.client.execute(query, first_args)
        return self.client.execute(*args)

    def get_all_locations(self, **kwargs):
        # filters = {}
        # query_variables = []
        # if kwargs.get('user_ids'):
        #     query_variables.append('user_ids')
        #     filters['user_ids'] = kwargs.get('user_ids')

        # if kwargs.get('state'):
        #     query_variables.append('state')
        #     filters['state'] = kwargs.get('state')
        result = self._execute(
            self.all_locations_query(kwargs.keys()),
            kwargs, "getAllLocations")
        return self._get_response(result, "all_locations")

    def get_locations(self, user_id):
        result = self._execute(
            self.location_query(),
            {"user_id": user_id}, "getUserLocations")
        return self._get_response(result, "user_locations")

    def get_states(self):
        result = self._execute(
            self.states_query(),
            {}, "getStates")
        return self._get_response(result, "states")

    def get_constituencies(self):
        result = self._execute(
            self.constituencies_query(),
            {}, "getConstituencies")
        return self._get_response(result, "constituencies")


class LocationAPIClient(BaseClient):
    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")
        self.service = LocationGraph(self.base_url)
        self.get_request_class()
    
    def get_all_locations(self, **kwargs):
        result = self.service.get_all_locations(**kwargs)
        return result

    def get_locations(self, user_id):
        result = self.service.get_locations(user_id)
        return result

    def get_states(self):
        result = self.service.get_states()
        return result

    def get_constituencies(self):
        result = self.service.get_constituencies()
        return result

    def update_users_location(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}update_users_location/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)
    
    def resolve_path(self, **kwargs):
        path = "locations/"
        sid = kwargs.pop('location_id', '')
        if sid:
            path = '{}{}/'.format(path, sid)
        return path, kwargs
