import warnings

from .base import BaseClient, TuteriaApiException, get_field_value
from nameko.standalone.rpc import ClusterRpcProxy


class BookingAPIClient(BaseClient):

    def __init__(self, order, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")
        self.order = order

    def get_weekday_elapsed(self):
        data = {
            'name': 'booking',
            'key': "order",
            'value': "%s" % self.order,
            "fields": ["weeks_elapsed", ]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        temp = response['booking']
        return temp['weeks_elapsed']

    def get_tutor_level(self, email):
        data = {
            'name': 'tutor',
            'key': "email",
            'value': "%s" % email,
            "fields": ["admin_cut"]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        return response['tutor']['admin_cut']

    def pay_tutor(self, amount_completed, refund, cancel_initiator=None):
        with ClusterRpcProxy(self.config) as rpc:
            rpc.payment_service.pay_tutor_with_booking.async(order=self.order,
                                                             amount_completed=amount_completed, refund=refund, cancel_initiator=cancel_initiator)

    @classmethod
    def create_payment_wallet_transaction(cls, booking, broker_url=None):
        if broker_url:
            cls.config = {'AMQP_URI': broker_url}
        with ClusterRpcProxy(cls.config) as rpc:
            rpc.payment_service.create_payment_wallet_transaction.async(
                **booking)

    def after_booking_has_been_paid(self):
        with ClusterRpcProxy(self.config) as rpc:
            rpc.payment_service.after_booking_has_been_paid.async(
                order=self.order)

    def get_booking_urls(self):
        data = {
            'name': 'booking',
            'key': "order",
            'value': self.order,
            "fields": ["get_absolute_url", "get_tutor_absolute_url"]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        return response['booking']
