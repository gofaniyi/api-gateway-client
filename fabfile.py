import os
from fabric.api import local, run, cd, env, sudo, settings, lcd
from fabric.decorators import hosts


def local_env(command):
    with lcd('.'):
        local(command)

def run_tests():
    tests = '{0}.{1}_booking{2}'
    tests += '{0}.{1}_client_requests{2}'
    tests += '{0}.{1}_email{2}'
    tests += '{0}.{1}_graphql{2}'
    tests += '{0}.{1}_image{2}'
    tests += '{0}.{1}_location{2}'
    tests += '{0}.{1}_main{2}'
    tests += '{0}.{1}_payments{2}'
    tests += '{0}.{1}_quiz{2}'
    tests += '{0}.{1}_request_pool{2}'
    tests += '{0}.{1}_tutor{2}'
    tests += '{0}.{1}_user{2}'
    tests += '{0}.{1}_user1{2}'
    tests += '{0}.{1}_reward{2}'
    local_env("python -m unittest " + tests.format('tests', 'test', ' '))