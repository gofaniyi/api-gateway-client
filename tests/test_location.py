import unittest
try:
    from unittest import mock
except ImportError:
    import mock
from gateway_client.location import LocationAPIClient
from gateway_client import FormField
from . import BaseApiTestCase
import json
from gateway_client.client3 import to_graphql

API_RESPONSE = '''{"form_fields": {
        "address": { "type": "CharField", "kwargs": { "required": true } },
        "area" : { "type": "CharField", "kwargs": { "required": true } },
        "state": {
            "type": "TypedChoiceField",
            "kwargs": { "required": true,
                "choices": [
                    [ "", "Select State" ], [ "Abia", "Abia" ], [ "Abuja", "Abuja" ],
                    [ "Adamawa", "Adamawa" ], [ "Akwa Ibom", "Akwa Ibom" ], [ "Anambra", "Anambra" ],
                    [ "Bayelsa", "Bayelsa" ], [ "Bauchi", "Bauchi" ], [ "Benue", "Benue" ],
                    [ "Borno", "Borno" ], [ "Cross River", "Cross River" ], [ "Delta", "Delta" ],
                    [ "Edo", "Edo" ], [ "Ebonyi", "Ebonyi" ], [ "Ekiti", "Ekiti" ],
                    [ "Enugu", "Enugu" ], [ "Gombe", "Gombe" ], [ "Imo", "Imo" ],
                    [ "Jigawa", "Jigawa" ], [ "Kaduna", "Kaduna" ], [ "Kano", "Kano" ],
                    [ "Katsina", "Katsina" ], [ "Kebbi", "Kebbi" ], [ "Kogi", "Kogi" ],
                    [ "Kwara", "Kwara" ], [ "Lagos", "Lagos" ], [ "Nassawara", "Nassawara" ],
                    [ "Niger", "Niger" ], [ "Ogun", "Ogun" ], [ "Ondo", "Ondo" ],
                    [ "Osun", "Osun" ], [ "Oyo", "Oyo" ], [ "Plateau", "Plateau" ],
                    [ "Rivers", "Rivers" ], [ "Sokoto", "Sokoto" ], [ "Taraba", "Taraba" ],
                    [ "Yobe", "Yobe" ], [ "Zamfara", "Zamfara" ] ]
            }
        },
        "vicinity": { "type": "CharField", "kwargs": { "required": true } }
    }}'''


class LocationAPIClientTestCase(BaseApiTestCase):
    def setUp(self):
        self.patcher = mock.patch('gateway_client.requests.get')
        self.patcher2 = mock.patch('gateway_client.requests.post')
        self.patcher4 = mock.patch('gateway_client.requests.patch')
        self.pather3 = mock.patch(
            'gateway_client.requests.options')
        self.mock_get = self.patcher.start()
        self.mock_post = self.patcher2.start()
        self.mock_options = self.pather3.start()
        self.mock_patch = self.patcher4.start()
        self.instance = LocationAPIClient('http://localhost:5000/')

    def tearDown(self):
        self.patcher.stop()
        self.patcher2.stop()
        self.pather3.stop()
        self.patcher4.stop()

    def test_get_location(self):
        resultss = {"vicinity": "Abuja", "state": "Lagos", "area" : "Allen Avenue"}
        self.mock_post.return_value = self.mock_response(
            {'user_locations': resultss})
        a = LocationAPIClient("http://localhost:3000/")
        result = a.get_locations(234)
        data = {
            'name': 'location',
            'key': "email",
            'value': "a@example.com",
            "fields": ["state", "vicinity", "area"]
        }
        self.mock_post.assert_called_once_with(
            "http://localhost:3000/graphql",
            data=json.dumps({
                'query': to_graphql(a.service.location_query()),
                'operationName': "getUserLocations",
                'variables': {'user_id': 234},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, resultss)


    def test_get_states(self):
        resultss = ['Lagos', 'Osun', 'Oyo']
        self.mock_post.return_value = self.mock_response(
            {'states': resultss})
        a = LocationAPIClient("http://localhost:3000/")
        result = a.get_states()
        
        self.mock_post.assert_called_once_with(
            "http://localhost:3000/graphql",
            data=json.dumps({
                'query': to_graphql(a.service.states_query()),
                'operationName': "getStates",
                'variables': {},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, resultss)


    def test_get_constituencies(self):
        resultss = [{
            'name' : 'Kosofe',
            'state' : 'Lagos'
        }, {
            'name' : 'Ibafo',
            'state' : 'Ogun'
        }]
        self.mock_post.return_value = self.mock_response(
            {'constituencies': resultss})
        a = LocationAPIClient("http://localhost:3000/")
        result = a.get_constituencies()
        
        self.mock_post.assert_called_once_with(
            "http://localhost:3000/graphql",
            data=json.dumps({
                'query': to_graphql(a.service.constituencies_query()),
                'operationName': "getConstituencies",
                'variables': {},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, resultss)


    def test_get_all_locations(self):
        resultss = [{
            'user_id' : 20,
            'location' : {'address' : 'Lagos street'}
        }, {
            'user_id' : 22,
            'location' : {'address' : 'Ibadan street'}
        }]
        self.mock_post.return_value = self.mock_response(
            {'all_locations': resultss})
        a = LocationAPIClient("http://localhost:3000/")
        result = a.get_all_locations()
        
        self.mock_post.assert_called_once_with(
            "http://localhost:3000/graphql",
            data=json.dumps({
                'query': to_graphql(a.service.all_locations_query()),
                'operationName': "getAllLocations",
                'variables': {},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, resultss)

    def test_get_all_locations_with_filters(self):
        resultss = [{
            'user_id' : 20,
            'location' : {'address' : 'Lagos street'}
        }, {
            'user_id' : 22,
            'location' : {'address' : 'Ibadan street'}
        }]
        self.mock_post.return_value = self.mock_response(
            {'all_locations': resultss})
        a = LocationAPIClient("http://localhost:3000/")
        result = a.get_all_locations(user_ids=[20,22])
        
        self.mock_post.assert_called_once_with(
            "http://localhost:3000/graphql",
            data=json.dumps({
                'query': to_graphql(a.service.all_locations_query(['user_ids'])),
                'operationName': "getAllLocations",
                'variables': {'user_ids' : [20,22]},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, resultss)


    def test_form_parameters_is_successfully_fetched(self):
        self.mock_options.return_value = self.mock_response(
            json.loads(API_RESPONSE), overwrite=True)
        result = self.instance.get_form_parameters()
        self.assertIsInstance(result, FormField)
        self.assertListEqual(
            sorted(result.get_fields()),
            sorted(['address', 'state', 'vicinity', 'area']))

    def test_validation_sends_required_params(self):
        self.instance.validate(
            dict({'account_id': "00032323323"}), home_form=True)
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/locations/validate_form/',
            params={'home_form': True}, json={'account_id': "00032323323"})

    def test_form_can_be_saved_successfully(self):
        response = {
            'name': 'Abiola'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_form(
            dict({'account_id': "00032323323"}), form_type='parent_home_form')
        self.assertEqual(result, response)

    def test_form_can_be_saved_successfully(self):
        response = {
            'name': 'Abiola'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_form(
            dict({'account_id': "00032323323"}), form_type='parent_home_form')
        self.assertEqual(result, response)

    def test_saving_form_sends_required_parameters(self):
        response = {
            'name': 'Abiola'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)

        result = self.instance.save_form(
            {'account_id': "00032323323"},
            form_type='parent_home_form')
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/locations/save_form/',
            params={'form_type': 'parent_home_form'},
            json={'account_id': "00032323323"})


    def test_users_location_updated_successfully(self):
        response = [
            {'user_id' : 2, 'location' : {'address' : 'Business', 'region' : 2}},
            {'user_id' : 3, 'location' : {'address' : 'Locality', 'region' : 2}},
        ]
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.update_users_location(
            [
            {'user_id' : 2, 'region' : 2},
            {'user_id' : 3, 'region' : 2},
        ]
        )
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/locations/update_users_location/',
            params={},
            json=[
            {'user_id' : 2, 'region' : 2},
            {'user_id' : 3, 'region' : 2},
        ])

        self.assertEqual(result, response)    
