import unittest
import json
import os
from . import mock

from gateway_client.email import email_and_sms_helper, TuteriaApiException


class EmailServiceAPITestCase(unittest.TestCase):

    def setUp(self):
        self.booking = dict(
            to="gbozee@gmail.com",
            title="Delay in Payment",
            template='delayed_payments',
            service='payment',
            context={})
        self.sms_options = dict(
            sender="+2347035209988",
            receiver="+2347035209988",
            body="Hello from Biola",
            client_type="infobib"
        )
        self.url = 'http://localhost:5000'

    @mock.patch("gateway_client.email.os.getenv")
    def test_raises_exception_when_no_url_is_passed(self, mock_get_env):
        mock_get_env.return_value = ""
        with self.assertRaisesRegexp(TuteriaApiException,
                                     'Url for Email Service was not found. Please pass in a url through "url" parameter'):
            email_and_sms_helper(self.booking)

    @mock.patch("gateway_client.email.requests.post")
    def test_doesnt_raise_exception_when_environmental_variable_is_set(self, mock_post):
        os.environ.setdefault("EMAIL_SERVICE_URL", self.url)
        email_and_sms_helper(self.booking)
        mock_post.assert_called_once_with('%s/send_message/' % self.url,
                                          json=dict(
                                              to=["gbozee@gmail.com"],
                                              title="Delay in Payment",
                                              template='delayed_payments',
                                              backend='mailgun_backend',
                                              service='payment',
                                              from_mail='Tuteria <automated@tuteria.com>',
                                              context={})
                                          )

    @mock.patch("gateway_client.email.requests.post")
    def test_doesnt_raise_exception_when_url_is_passed(self, mock_post):
        email_and_sms_helper(self.booking, url=self.url)
        mock_post.assert_called_with('%s/send_message/' % self.url,
                                     json=dict(
                                         to=["gbozee@gmail.com"],
                                         title="Delay in Payment",
                                         template='delayed_payments',
                                         service='payment',
                                         backend='mailgun_backend',
                                         from_mail='Tuteria <automated@tuteria.com>',
                                         context={})
                                     )

    @mock.patch("gateway_client.email.requests.post")
    def test_email_and_sms_helper_works_when_only_booking_is_passed(self, mock_post):
        class MockResponse(object):
            pass
        mock_response = MockResponse()
        mock_response.raise_for_status = mock.MagicMock()
        mock_response.json = mock.MagicMock(
            return_value={'status': 'Successful'})
        mock_post.return_value = mock_response
        response = email_and_sms_helper(self.booking, url=self.url)
        mock_response.raise_for_status.assert_called_with()
        mock_response.json.assert_called_with()
        self.assertEqual(response, {'status': 'Successful'})

    @mock.patch("gateway_client.email.requests.post")
    def test_an_api_call_fails(self, mock_post):
        class MockResponse(object):
            pass
        mock_response = MockResponse()
        mock_response.raise_for_status = mock.Mock(
            side_effect=Exception("foo"))
        with self.assertRaises(Exception) as context:
            email_and_sms_helper(self.booking, url=self.url)
            self.assertTrue("foo" in context.exception)

    @mock.patch("gateway_client.email.requests.post")
    def test_email_and_sms_is_send(self, mock_post):
        email_and_sms_helper(
            self.booking, sms_options=self.sms_options, url=self.url)
        mock_post.assert_called_with(
            '%s/send_message/' % self.url,
            json=dict(
                to=["gbozee@gmail.com"],
                title="Delay in Payment",
                template='delayed_payments',
                service='payment',
                from_mail='Tuteria <automated@tuteria.com>',
                backend='mailgun_backend',
                context={},
                sms_options=dict(
                    receiver="+2347035209988",
                    body="Hello from Biola",
                    client_type="infobib"
                ),)
        )

    @mock.patch("gateway_client.email.requests.post")
    def test_called_with_backend_works_correctly(self, mock_post):
        email_and_sms_helper(
            self.booking, sms_options=self.sms_options, url=self.url,
            backend='mandril_backend')
        mock_post.assert_called_with(
            '%s/send_message/' % self.url,
            json=dict(
                to=["gbozee@gmail.com"],
                title="Delay in Payment",
                template='delayed_payments',
                service='payment',
                from_mail='Tuteria <automated@tuteria.com>',
                context={},
                backend='mandril_backend',
                sms_options=dict(
                    receiver="+2347035209988",
                    body="Hello from Biola",
                    client_type="infobib"
                ),)
        )

    @mock.patch("gateway_client.email.requests.post")
    def test_called_without_template(self, mock_post):
        email_and_sms_helper(
            dict(to="gbozee@gmail.com",
                 title="Delay in Payment",
                 body='James Kovac'),
            url=self.url,
            backend='mandril_backend')
        mock_post.assert_called_with(
            '%s/send_message/' % self.url,
            json=dict(
                to=["gbozee@gmail.com"],
                title="Delay in Payment",
                body='James Kovac',
                backend='mandril_backend',
                from_mail='Tuteria <automated@tuteria.com>')
        )

    @mock.patch("gateway_client.email.requests.post")
    def test_called_with_backend_is_not_used_correctly(self, mock_post):
        email_and_sms_helper(
            self.booking, sms_options=self.sms_options, url=self.url,
            backend=object())
        mock_post.assert_called_with(
            '%s/send_message/' % self.url,
            json=dict(
                to=["gbozee@gmail.com"],
                title="Delay in Payment",
                template='delayed_payments',
                service='payment',
                from_mail='Tuteria <automated@tuteria.com>',
                backend='mailgun_backend',
                context={},
                sms_options=dict(
                    receiver="+2347035209988",
                    body="Hello from Biola",
                    client_type="infobib"
                ),)
        )

    @mock.patch("gateway_client.email.requests.post")
    def test_called_with_backend_is_not_passed(self, mock_post):
        email_and_sms_helper(
            self.booking, sms_options=self.sms_options, url=self.url,
        )
        mock_post.assert_called_with(
            '%s/send_message/' % self.url,
            json=dict(
                to=["gbozee@gmail.com"],
                title="Delay in Payment",
                template='delayed_payments',
                service='payment',
                from_mail='Tuteria <automated@tuteria.com>',
                backend='mailgun_backend',
                context={},
                sms_options=dict(
                    receiver="+2347035209988",
                    body="Hello from Biola",
                    client_type="infobib"
                ),)
        )
