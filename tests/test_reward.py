import unittest
try:
    from unittest import mock
except ImportError:
    import mock
from gateway_client.rewards import RewardAPIClient
from gateway_client import FormField
from . import BaseApiTestCase
import json
from gateway_client.client3 import to_graphql


class RewardAPIClientTestCase(BaseApiTestCase):
    def setUp(self):
        self.patcher = mock.patch('gateway_client.requests.get')
        self.patcher2 = mock.patch('gateway_client.requests.post')
        self.patcher4 = mock.patch('gateway_client.requests.patch')
        self.pather3 = mock.patch(
            'gateway_client.requests.options')
        self.mock_get = self.patcher.start()
        self.mock_post = self.patcher2.start()
        self.mock_options = self.pather3.start()
        self.mock_patch = self.patcher4.start()
        self.instance = RewardAPIClient('http://localhost:5000/')
        self.url = 'http://localhost:5000'

    def tearDown(self):
        self.patcher.stop()
        self.patcher2.stop()
        self.pather3.stop()
        self.patcher4.stop()

    def test_get_cached_milestones(self):
        response = {
            "cached_milestones" : [
                {'condition' : 2},
                {'condition' : 3}
            ]
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_milestones()
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.cached_milestones_query()),
                'operationName': "getMilestones",
                'variables': {},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['cached_milestones'])
