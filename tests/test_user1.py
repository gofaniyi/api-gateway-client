import unittest
try:
    from unittest import mock
except ImportError:
    import mock
from gateway_client.user import UserAPIClient
from gateway_client import FormField
from . import BaseApiTestCase
import json
from gateway_client.client3 import to_graphql


class UserAPIClientTestCase(BaseApiTestCase):
    def setUp(self):
        self.patcher = mock.patch('gateway_client.requests.get')
        self.patcher2 = mock.patch('gateway_client.requests.post')
        self.patcher4 = mock.patch('gateway_client.requests.patch')
        self.pather3 = mock.patch(
            'gateway_client.requests.options')
        self.mock_get = self.patcher.start()
        self.mock_post = self.patcher2.start()
        self.mock_options = self.pather3.start()
        self.mock_patch = self.patcher4.start()
        self.instance = UserAPIClient('http://localhost:5000/')
        self.url = 'http://localhost:5000'

    def tearDown(self):
        self.patcher.stop()
        self.patcher2.stop()
        self.pather3.stop()
        self.patcher4.stop()

    def test_get_cached_user(self):
        response = {
            "cached_user" : {
                "id": "312",
                "first_name" : "John",
                "last_name" : "Brain"
            }
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_user(email='a@example.com')
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.cached_user_query({'email' : 'a@example.com'})),
                'operationName': "getUser",
                'variables': {'email' : 'a@example.com'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['cached_user'])


    def test_get_cached_user_locations(self):
        response = {
            "cached_user_locations" : [
                {'addr_type' : 2},
                {'addr_type' : 1}
            ]
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_user_locations(id=20)
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.cached_user_locations_query({'id' : 20})),
                'operationName': "getUserLocations",
                'variables': {'id' : 20},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['cached_user_locations'])


    def test_get_cached_tutor_skills(self):
        response = {
            "cached_tutor_skills" : [
                {'status' : 2},
                {'status' : 1}
            ]
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_tutor_skills(id=20)
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.cached_tutor_skills_query({'id' : 20})),
                'operationName': "getTutorSkills",
                'variables': {'id' : 20},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['cached_tutor_skills'])

    
    def test_get_cached_user_bookings(self):
        response = {
            "cached_user_bookings" : [
                {'status' : 2},
                {'status' : 1}
            ]
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_user_bookings(id=20)
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.cached_user_bookings_query({'id' : 20})),
                'operationName': "getUserBookings",
                'variables': {'id' : 20},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['cached_user_bookings'])


    def test_get_cached_user_orders(self):
        response = {
            "cached_user_orders" : [
                {'status' : 2},
                {'status' : 1}
            ]
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_user_orders(id=20)
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.cached_user_orders_query({'id' : 20})),
                'operationName': "getUserOrders",
                'variables': {'id' : 20},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['cached_user_orders'])

    
    def test_can_populate_booking_first_session(self):
        response = {
            "populate_booking_first_session" : {
                'booking' : {
                    'status' : 1
                }
            }
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.populate_booking_first_session(order="2MNH11TT", user_id="20")
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.populate_booking_first_session_query()),
                'operationName': None,
                'variables': dict(order="2MNH11TT", user_id="20"),
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['populate_booking_first_session'])