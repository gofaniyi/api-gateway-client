import unittest
try:
    from unittest import mock
except ImportError:
    import mock
from gateway_client import construct_graphql_query, get_field_value


class MockRequest:

    def __init__(self, response, **kwargs):
        self.response = response
        self.overwrite = False
        if kwargs.get('overwrite'):
            self.overwrite = True
        self.status_code = kwargs.get('status_code', 200)

    @classmethod
    def raise_for_status(cls):
        pass

    def json(self):
        if self.overwrite:
            return self.response
        return {'data': self.response}


class BaseApiTestCase(unittest.TestCase):
    module_name = ""

    def setUp(self):
        self.patcher = mock.patch('gateway_client.requests.post')
        self.mock_post = self.patcher.start()
        self.construct_patch()

    def tearDown(self):
        if self.module_name:
            self.construct_tearDown()
        else:
            self.patcher.stop()

    def construct_patch(self):
        def module_type(x, module_name=self.module_name):
            if module_name == 'None':
                return 'gateway_client.requests.{}'.format(x)
            return 'gateway_client.{}.requests.{}'.format(
                module_name, x)
        if self.module_name:
            self.patcher = mock.patch(module_type('get'))
            self.patcher2 = mock.patch(module_type('post'))
            self.patcher4 = mock.patch(module_type('patch'))
            self.pather3 = mock.patch(
                module_type('options', "client_requests"))
            self.mock_get = self.patcher.start()
            self.mock_post = self.patcher2.start()
            self.mock_options = self.pather3.start()
            self.mock_patch = self.patcher4.start()

    def construct_tearDown(self):
        if self.module_name:
            self.patcher.stop()
            self.patcher2.stop()
            self.pather3.stop()
            self.patcher4.stop()

    def graphql_request(self, data):
        return {
            'query': data,
            'variables': None
        }

    def construct_graphql_query(self, data):
        return construct_graphql_query(get_field_value(data))

    def graphql_query(self, data):
        return self.graphql_request(self.construct_graphql_query(data))

    def mock_response(self, data, **kwargs):
        return MockRequest(data, **kwargs)
