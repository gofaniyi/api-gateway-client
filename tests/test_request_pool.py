from . import unittest
from . import mock
import graphene
from gateway_client.request_pool import MatchingService
from gateway_client import TuteriaApiException, GraphQLClient


class TutorNode(graphene.ObjectType):
    first_name = graphene.String()
    last_name = graphene.String()
    username = graphene.String()

    def resolve_username(self, *args):
        return self['username']

    def resolve_first_name(self, *args):
        return self['first_name']

    def resolve_last_name(self, *args):
        return self['last_name']


class SampleNode(graphene.ObjectType):
    id = graphene.Int()
    tutor = graphene.Field(TutorNode)
    subjects = graphene.List(graphene.String)
    cost = graphene.Float()
    recommended = graphene.Boolean()
    request_status = graphene.String()
    default_subject = graphene.String()
    approved = graphene.Boolean()
    req_id = graphene.Int()

    def resolve_id(self, *args):
        return self['id']

    def resolve_tutor(self, *args):
        return self['tutor']

    def resolve_subjects(self, *args):
        return self['subjects']

    def resolve_cost(self, *args):
        return self['cost']

    def resolve_recommended(self, *args):
        return self['recommended']

    def resolve_default_subject(self, *args):
        return self['default_subject']

    def resolve_approved(self, *args):
        return self['approved']

    def resolve_req_id(self, *args):
        return self['req_id']


sample_record = [
    {
        'id': 23,
        'tutor': {
            'first_name': "John",
            'last_name': "Doe",
            'username': 'johndoe'
        },
        "subjects": ['English', "Mathematics"],
        "cost":20000,
        "recommended":False,
        "default_subject":"",
        "approved":True
    }]


class AddToPoolMutation(graphene.Mutation):
    class Input:
        tutor_slug = graphene.String()
        cost = graphene.Float()
        request_subjects = graphene.List(graphene.String)
        notify_job = graphene.Boolean()
        req_id = graphene.Int()

    pool_instance = graphene.Field(SampleNode)

    @staticmethod
    def mutate(root, args, *extra):
        instance = sample_record[0]
        instance['cost'] = args['cost']
        return AddToPoolMutation(pool_instance=instance)


class CreatePoolMutation(graphene.Mutation):
    class Input:
        tutor_slug = graphene.String()
        approved = graphene.Boolean()
        req_id = graphene.Int()
        create = graphene.Boolean()

    pool_instance = graphene.Field(SampleNode)

    @staticmethod
    def mutate(root, args, *extra):
        instance = sample_record[0]
        return CreatePoolMutation(pool_instance=instance)


class Mutation(graphene.ObjectType):
    create_dummy_pool = CreatePoolMutation.Field()
    add_tutor_to_pool = AddToPoolMutation.Field()


class Query(graphene.ObjectType):
    pool_instance = graphene.Field(
        SampleNode,
        request_id=graphene.Int(required=True),
        tutor_slug=graphene.String())

    approved_tutors = graphene.List(
        SampleNode,
        request_id=graphene.Int(required=True))

    approved_tutors_teach_all_subjects = graphene.List(
        SampleNode,
        request_id=graphene.Int(required=True))

    def resolve_pool_instance(self, args, *extra):
        return sample_record[0]

    def resolve_approved_tutors(self, args, context, info):
        new_result = []
        for o in sample_record:
            o.update(req_id=args['request_id'])
            new_result.append(o)
        return new_result

    def resolve_approved_tutors_teach_all_subjects(self, args, *remaining):
        new_result = []
        for o in sample_record:
            o.update(req_id=args['request_id'])
            new_result.append(o)

        return new_result


class BaseTestCase(object):
    def setUp(self):
        self.fields = """
            id
            tutor{
                first_name
                last_name
            }
            req_id
        """
        self.query_string = lambda x=32: """
        {
            approved_tutors(request_id:%s){
                %s
            }
        }
        """ % (x, self.fields)

    def test_service_can_receive_a_schema_object(self):

        self.assertEqual(self.service.client, self.schema)

    def test_service_throws_error_when_no_url_is_passed(self):
        with self.assertRaises(TuteriaApiException):
            service = MatchingService()

    def test_actual_service_call_returns_valid_result_on_approve_tutors(self):
        pool_instance = self.service.init_instance(28)
        self.service.fields = self.fields
        result = self.service.approved_tutors()
        self.assertEqual(self.service.fields, self.fields)
        self.assertEqual(result[0]['req_id'], 28)

    def test_approved_tutors_teach_all_subjects(self):
        pool_instance = self.service.init_instance(28)
        self.service.fields = self.fields
        result = self.service.approved_tutors_teach_all_subjects()
        self.assertEqual(self.service.fields, self.fields)
        self.assertEqual(result[0]['req_id'], 28)

    def test_creation_of_dummy_data(self):
        self.service.init_instance(28)
        self.service.fields = self.fields
        result = self.service.create_dummy_pool(tutor_slug="dandolo",
                                                approved=True)
        self.assertEqual(result['pk'], 23)
        self.assertEqual(result['tutor_slug'], "johndoe")

    def test_fetching_of_request_pool_containing_tutor(self):
        self.service.init_instance(28)
        self.service.fields = self.fields
        result = self.service.get_tutor_for_request("johndoe")
        self.assertEqual(result['pk'], 23)
        self.assertEqual(result['tutor_slug'], "johndoe")

    def test_create_and_add_tutor_to_pool(self):
        self.service.init_instance(28)
        self.service.fields = self.fields
        result = self.service.create_and_add_tutor_to_pool(
            request_subjects=["English Language"],
            tutor_slug="johndoe",
            cost=7000
        )
        self.assertEqual(result['id'], 23)
        self.assertEqual(result['tutor']['username'], 'johndoe')
        self.assertEqual(result['cost'], 7000)


class LocalMatchingServiceTestCase(BaseTestCase, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.schema = graphene.Schema(
            query=Query, mutation=Mutation, auto_camelcase=False)
        self.service = MatchingService(self.schema)

    def test_above_schema_works_well(self):
        result = self.schema.execute(self.query_string())
        self.assertIsNone(result.errors)


class RemoteMatchingServiceTestCase(BaseTestCase, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.patcher = mock.patch.object(GraphQLClient, "execute")
        self.mocker = self.patcher.start()
        ss = sample_record[0].copy()
        ss.update(req_id=28)
        so = ss.copy()
        so.update(cost=7000)
        self.mocker.return_value = {
            'data': {
                'approved_tutors': [ss],
                'approved_tutors_teach_all_subjects': [ss],
                'create_dummy_pool': {
                    'pool_instance': ss
                },
                'add_tutor_to_pool': {
                    'pool_instance': so
                },
                'pool_instance': ss
            }
        }
        self.service = MatchingService("http://localhost:5000")
        self.schema = self.service.client

    def tearDown(self):
        self.patcher.stop()

    def test_instance_of_client_is_of_GraphQLClient(self):
        self.assertIsInstance(self.service.client, GraphQLClient)
        self.assertFalse(self.service.change)

    def test_creation_of_dummy_data(self):
        super().test_creation_of_dummy_data()
        self.mocker.assert_called_once_with(self.service.query, dict(tutor_slug="dandolo",
                                                                     approved=True, req_id=28), "createDummyPool")

    def test_create_and_add_tutor_to_pool(self):
        super().test_create_and_add_tutor_to_pool()

    def test_fetching_of_request_pool_containing_tutor(self):
        super().test_fetching_of_request_pool_containing_tutor()
        self.mocker.assert_called_once_with(self.service.query, {
            'request_id': 28,
            'tutor_slug': 'johndoe'
        }, "SingleRequestPool")
