from __future__ import unicode_literals
from builtins import super

import unittest
try:
    from unittest import mock
except ImportError:
    import mock
from gateway_client.booking import BookingAPIClient
from . import BaseApiTestCase


class BookingAPIClientTestCase(BaseApiTestCase):

    def setUp(self):
        super().setUp()
        self.order = u'P21J7B8G48LR'
        self.a = BookingAPIClient(self.order, "http://localhost:3000")

    @mock.patch("gateway_client.booking.ClusterRpcProxy")
    def test_notifies_service_to_create_wallet(self, mock_client):
        instance = mock_client.return_value.__enter__.return_value
        booking_details = {
            'order': u'P21J7B8G48LR',
            'total_price': 28000,
            'tutor': {
                'email': u'besidonny@gmail.com',
                'first_name': u'Besidone',
                'last_name': u'Obaroh',
                'username': u'besidoneo'
            },
            'user': {
                'email': u'rosiedoh79@yahoo.com',
                'first_name': u'Mrs Rosemary',
                'last_name': u'Ajato',
                'username': u'rosiedoh79'},
            'wallet_amount': 0
        }
        BookingAPIClient.create_payment_wallet_transaction(booking_details)
        instance.payment_service.create_payment_wallet_transaction.async\
            .assert_called_once_with(**booking_details)

    @mock.patch("gateway_client.booking.ClusterRpcProxy")
    def test_notifies_service_to_pay_tutor(self, mock_client):
        instance = mock_client.return_value.__enter__.return_value
        self.a.pay_tutor(12000, 0, True)
        instance.payment_service.pay_tutor_with_booking.async\
            .assert_called_once_with(order='P21J7B8G48LR', amount_completed=12000,
                                     refund=0, cancel_initiator=True)

    @mock.patch("gateway_client.booking.ClusterRpcProxy")
    def test_after_booking_has_been_placed_sends_correct_parameters(self, mock_client):
        instance = mock_client.return_value.__enter__.return_value
        self.a.after_booking_has_been_paid()
        instance.payment_service.after_booking_has_been_paid.async\
            .assert_called_once_with(order='P21J7B8G48LR')

    def test_get_tutor_level(self):
        resultss = {"admin_cut": 30}
        self.mock_post.return_value = self.mock_response({'tutor': resultss})
        result = self.a.get_tutor_level("a@example.com")
        data = {
            'name': 'tutor',
            'key': "email",
            'value': "a@example.com",
            "fields": ["admin_cut"]
        }
        self.mock_post.assert_called_once_with("http://localhost:3000",
                                               json=self.graphql_query(data))
        self.assertEqual(result, 30)

    def test_get_absolute_url(self):
        resultss = {'get_absolute_url': '/bookings/manage_bookings/ABCDEFGHIJKL',
                    "get_tutor_absolute_url": '/bookings/ABCDEFGHIJKL'}
        self.mock_post.return_value = self.mock_response(
            dict(booking=resultss))
        result = self.a.get_booking_urls()
        data = {
            'name': 'booking',
            'key': "order",
            'value': self.order,
            "fields": ["get_absolute_url", "get_tutor_absolute_url"]
        }
        self.mock_post.assert_called_once_with("http://localhost:3000",
                                               json=self.graphql_query(data))
        self.assertEqual(result, resultss)
